extends Control

enum {
	TURN_NEW,
	TURN_FIRST_SELECTED,
	TURN_SECOND_SELECTED,
	TURN_RESOLVING,
	TURN_GAMEOVER,
	TURN_VICTORY,
}

const MAP_WIDTH = 8
const MAP_HEIGHT = 8
const STARTING_TREASURES = 10
const STARTING_MONSTERS = 2
const STARTING_TILES = {
	Tile.TILE_TYPE_BAG: 10,
	Tile.TILE_TYPE_COIN: 46,
	Tile.TILE_TYPE_SWORD: 4,
	Tile.TILE_TYPE_ARMOR: 2,
	Tile.TILE_TYPE_MONSTER: STARTING_MONSTERS,
}

var TileScene = preload("res://Tile.tscn")
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var _turn_phase = TURN_NEW
var _card1 = null
var _card2 = null
var _treasures = 0
var _weapons = 0
var _armor = 0
var _monsters = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	$Playfield.columns = MAP_WIDTH
	_set_playfield()
	_set_treasures(STARTING_TREASURES)
	_set_monsters(STARTING_MONSTERS)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _set_playfield():
	var tilelist = []
	for k in STARTING_TILES.keys():
		for _i in range(STARTING_TILES[k]):
			var t = TileScene.instance()
			t.configure(k)
			tilelist.append(t)
	tilelist.shuffle()
	
	for t in tilelist:
		t.connect("tile_clicked", self, "_on_tile_clicked")
		t.connect("fade_completed", self, "_on_tile_fade_completed")
		$Playfield.add_child(t)

func _on_tile_clicked(tile):
	match _turn_phase:
		TURN_NEW:
			_card1 = tile
			tile.expose()
			_turn_phase = TURN_FIRST_SELECTED
			_set_treasures(_treasures - 1)
		TURN_FIRST_SELECTED:
			_card2 = tile
			tile.expose()
			_turn_phase = TURN_SECOND_SELECTED
		_:
			pass
			
func _on_tile_fade_completed(tile):
	if _turn_phase == TURN_SECOND_SELECTED && tile == _card2:
		_turn_phase = TURN_RESOLVING
		$ResolveTimer.start()


func _on_ResolveTimer_timeout():
	if _card1.tile_type == _card2.tile_type:
		var disable = true
		match _card1.tile_type:
			Tile.TILE_TYPE_COIN:
				_set_treasures(_treasures + 1)
			Tile.TILE_TYPE_BAG:
				_set_treasures(_treasures + 5)
			Tile.TILE_TYPE_SWORD:
				_set_weapons(_weapons + 1)
			Tile.TILE_TYPE_ARMOR:
				_set_armor(_armor + 1)
			Tile.TILE_TYPE_MONSTER:
				if _weapons > 0:
					_set_treasures(_treasures + 20)
					_set_weapons(_weapons - 1)
					_set_monsters(_monsters - 2)
				elif _armor > 0:
					_set_armor(_armor - 1)
					disable = false
				else:
					_set_treasures(_treasures - 10)
					disable = false
			_:
				pass
		if disable:
			_card1.disable()
			_card2.disable()
		_turn_phase = TURN_NEW
	else:
		if _card1.tile_type != Tile.TILE_TYPE_MONSTER and _card2.tile_type != Tile.TILE_TYPE_MONSTER:
			_card1.reset()
			_card2.reset()
		else:
			if _card1.is_weapon() or _card2.is_weapon():
				_set_treasures(_treasures + 10)
				_set_monsters(_monsters - 1)
				_card1.disable()
				_card2.disable()
			elif _card1.is_armor() or _card2.is_armor():
				if _card1.is_armor():
					_card1.disable()
					_card2.reset()
				else:
					_card1.reset()
					_card2.disable()
			else:
				_set_treasures(_treasures - 5)
				_card1.reset()
				_card2.reset()

	_turn_phase = TURN_NEW
	_card2 = null
	_card1 = null
	if _treasures <= 0:
		$GameOver.show()
		_turn_phase = TURN_GAMEOVER
	if _monsters <= 0:
		$Victory.show()
		_turn_phase = TURN_VICTORY

func _set_armor(quantity):
	_armor = quantity
	$Armor/Quantity.text = str(_armor)

func _set_monsters(quantity):
	_monsters = quantity

func _set_treasures(quantity):
	_treasures = quantity
	$Treasures/Quantity.text = str(_treasures)

func _set_weapons(quantity):
	_weapons = quantity
	$Weapons/Quantity.text = str(_weapons)
